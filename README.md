# Frontend web project

project ini merupakan project submission pada kelas dicoding belajar dasar pemrograman web, tema dari project ini adalah mobil BMW seri 3.

[![Netlify Status](https://api.netlify.com/api/v1/badges/301bff8a-3456-414c-809e-4c544de009d2/deploy-status)](https://app.netlify.com/sites/hrzn-a123/deploys)

## Kriteria Submission

- Terdapat elemen `<header>`, `<footer>`, `<main>`, `<article>`, dan `<aside>` di berkas HTML.
- Masing-masing elemen wajib berisi konten yang peruntukkannya sesuai dengan elemen tersebut (menerapkan konsep semantic HTML dalam menyusun struktur website). Sebagai contoh: Header berisi judul dan navigation. Sedangkan konten artikel tidak boleh berada pada Header.
- Wajib menampilkan identitas diri (biodata diri) yang minimal harus berisi foto asli diri dan nama sesuai profil Dicoding. Identitas diri wajib ditampilkan dalam elemen `<aside>`.
- Menyusun layout dengan menggunakan float atau flexbox.
- Tema yang ditampilkan bebas, kecuali tema Bandung.
- Semakin detail dan lengkap website Anda maka nilai submission bisa lebih tinggi.

## Saran agar mendapat nilai tinggi

- Menerapkan tampilan aplikasi yang menarik:

  - Memiliki pemilihan warna yang pas dengan tema aplikasi (Dalam memilih warna, Anda dapat memanfaatkan tools pemilihan warna seperti colorhunt.co).
  - Tata letak elemen yang pas. Contoh : Tidak ada konten yang bertumpuk.
  - Penggunaan font yang pas dengan tema.
  - Penerapan padding dan margin yang pas.

- Menerapkan layout yang responsif:

  - Menggunakan media query untuk menyesuaikan layout pada berbagai ukuran layar device. Pastikan untuk tidak terdapat konten yang menumpuk maupun keluar dari kontainer ketika dicoba pada dekstop, tablet, dan juga mobile.
  - Menerapkan flexbox dalam menyusun layout.

- Terdapat penerapan JavaScript dalam memanipulasi DOM, seperti: (pilih satu)
  - Membuat drop down.
  - Memanfaatkan logika seperti looping dalam menampilkan elemen dan konten.
  - Membuat slider.
  - dan lainnya yang mendukung tampilan website agar lebih hidup.
