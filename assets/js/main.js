import threeSeries from "./content.js";

const carName = document.querySelector(".title");
const carGen = document.querySelector(".subtitle");
const indicator = document.querySelector(".indicator");
const carDesc = document.querySelector("p");
const carImgContainer = document.querySelector(".pic");

function showContent(i) {
  carImgContainer.src = `${threeSeries[i].img}`;

  indicator.innerText = `${i + 1} of ${threeSeries.length}`;

  carName.innerText = threeSeries[i].name;
  carGen.innerText = `Generasi ke-${threeSeries[i].gen}`;
  carDesc.innerText = threeSeries[i].desc;
}

function dispNextCar(index) {
  showContent(index);
  window.setTimeout(
    dispNextCar.bind(undefined, (index + 1) % threeSeries.length),
    12 * 1000
  );
}

dispNextCar(0);
