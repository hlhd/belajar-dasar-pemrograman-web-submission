class Car {
  constructor(name, gen, desc, imgLink) {
    this.name = name;
    this.gen = gen;
    this.desc = desc;
    this.img = imgLink;
  }
}

const e21 = new Car(
  "E21",
  1,
  "Saat diluncurkan, semua model menggunakan mesin 4-silinder karbuet;Namun, model yang disuntikkan bahan bakar diperkenalkan pada akhir tahun 1975 dan mesin 6 silinder ditambahkan pada tahun 1977. Gaya tubuh cabriolet-diproduksi oleh Baur-tersedia dari 1978 hingga 1981.",
  "https://cdn.bmwblog.com/wp-content/uploads/2020/07/The-E21-BMW-3-Series-13.jpg"
);

const e30 = new Car(
  "E30",
  2,
  'Pada pengantar pada tahun 1982, E30 diproduksi semata-mata dalam gaya tubuh sedan 2 pintu.Model sedan empat pintu diperkenalkan pada tahun 1983, convertible diperkenalkan pada tahun 1985 dan model Estate ("Touring") diperkenalkan pada tahun 1987. E30 adalah 3 seri pertama yang tersedia di Wagon dan gaya bodi sedan 4 pintu.Itu juga merupakan 3 seri pertama yang menawarkan mesin diesel, dan all-wheel drive diperkenalkan ke kisaran 3 seri dengan model 325ix.Roadster BMW Z1 didasarkan pada platform E30.',
  "https://car-images.bauersecure.com/pagefiles/97881/e30_m3_050.jpg"
);

const e36 = new Car(
  "E36",
  3,
  'E36 dijual dalam gaya tubuh berikut: sedan, coupé, convertible, wagon (dipasarkan sebagai "tur") dan hatchback (dipasarkan sebagai "3 seri compact"). E36 adalah 3 seri pertama yang ditawarkan dalam gaya tubuh hatchback.Itu juga merupakan 3 seri pertama yang tersedia dengan transmisi manual 6-kecepatan (pada 1996 M3), transmisi otomatis 5-kecepatan dan mesin diesel empat silinder.Suspensi belakang multi-link juga merupakan peningkatan yang signifikan dibandingkan dengan generasi sebelumnya dari 3 seri.',
  "https://s1.cdn.autoevolution.com/images/gallery/BMW-M3-Sedan--E36--771_18.jpg"
);

const e46 = new Car(
  "E46",
  4,
  'E46 dijual dalam gaya tubuh berikut: sedan, coupé, convertible, wagon (dipasarkan sebagai "tur") dan hatchback (dipasarkan sebagai "3 seri compact"). Generasi E46 memperkenalkan berbagai fitur elektronik ke seri 3, termasuk navigasi satelit, distribusi kekuatan rem elektronik, wiper penginderaan hujan dan lampu ekor LED.All-Wheel Drive, yang terakhir tersedia di E30 3 Series, diperkenalkan kembali untuk E46.Itu tersedia untuk model Sedan/Wagon 325xi dan 330xi.E46 adalah 3 seri pertama yang tersedia dengan mesin menggunakan variabel valve lift ("Valvetronic").',
  "https://cdn.bmwblog.com/wp-content/uploads/2016/10/BMW-E46-M3-phoenix-yellow-21.jpg"
);

const e90 = new Car(
  "E90",
  5,
  'Seri Generasi 3 Kelima diproduksi di sedan, Wagon, Coupé dan Cabriolet Body Styles.Karena kode model yang terpisah untuk setiap gaya tubuh, istilah "E9X" kadang -kadang digunakan untuk menggambarkan generasi ini dari seri 3. Pada tahun 2006, 335i menjadi model 3 seri pertama yang dijual dengan mesin bensin turbocharged.E90 juga melihat pengenalan ban run-flat ke rentang 3 seri.Akibatnya, mobil dengan run-flat tidak dilengkapi dengan ban cadangan.',
  "https://cdn.bmwblog.com/wp-content/uploads/2016/02/BMW-E90-M3-images-3.jpg"
);

const f30 = new Car(
  "F30",
  6,
  'F30/F31/F35 telah diproduksi di sedan, coupé, convertible, station wagon, dan gaya tubuh hatchback 5 pintu ("Gran Turismo").Sedan jarak sumbu roda panjang juga tersedia di Cina. Untuk seri F30/F31/F34, model coupe dan convertible diproduksi dari tahun 2013 hingga 2014 ketika mereka dibagi dari seri 3, didesain ulang, dan dijual sebagai BMW 4 Series.Gaya tubuh baru diperkenalkan ke dalam rentang 3 seri: 3 seri Gran Turismo, hatchback roda panjang.',
  "https://cdn.bmwblog.com/wp-content/uploads/2017/03/Estoril-Blue-BMW-F30-335i-Project-By-European-Auto-Source-15.jpg"
);

const g20 = new Car(
  "G20",
  7,
  "BMW 3 Series (G20) diluncurkan di Paris Motor Show 2018 pada 2 Oktober 2018. Gambar resmi kendaraan terungkap sehari sebelum pembukaannya.Generasi ketujuh dari seri 3 juga ditawarkan sebagai station wagon.Varian kompetisi M3 dan M3 yang lebih kuat disampaikan secara global mulai tahun 2021. G20 adalah generasi 3 seri pertama yang membawa tur M3 ke pasar.Sejak 2022, ia memperoleh versi listrik baterai yang eksklusif untuk pasar Cina sebagai i3, berbagi powertrainnya dengan I4 yang dipasarkan secara global.",
  "https://assets.autobuzz.my/wp-content/uploads/2018/10/02212648/2018-BMW-3series-G20-11.jpg"
);

const threeSeries = [e21, e30, e36, e46, e90, f30, g20];

export default threeSeries;
